#!/bin/bash
#SBATCH --job-name=EMMCAACCEPTPARAMETERS
#SBATCH --workdir=.
#SBATCH --output=output_%j.out
#SBATCH --error=output_%j.err
#SBATCH --ntasks=1
#SBATCH --qos=debug
#SBATCH --cpus-per-task=48
#SBATCH --time=01:59:00

srun -n1 -N1 --exclusive python3 accept_parameters.py --mode="pareto" --N=50000

wait


