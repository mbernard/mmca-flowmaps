#!/bin/bash
#SBATCH --job-name=EMMCA
#SBATCH --workdir=.
#SBATCH --output=output_%j.out
#SBATCH --error=output_%j.err
#SBATCH --ntasks=10
#SBATCH --qos=debug
#SBATCH --cpus-per-task=48
#SBATCH --time=01:59:00

export JULIA_NUM_THREADS=48

module load julia/1.3.1

INSTANCES=$(find ./instances -maxdepth 1 -mindepth 1 -type d -printf '%p\n')

for instance in $INSTANCES; do
	echo "Running instance $instance"
	srun -n1 -N1 --exclusive bash $instance/run.sh &
done

wait


