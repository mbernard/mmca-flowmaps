import pandas as pd
import numpy as np
import argparse
from scipy.stats import multivariate_normal


######################################
# Parser options
######################################
ap = argparse.ArgumentParser(description="")

ap.add_argument('--size', dest="size", required=True, type=int, help='Number of samples to generate')
args = ap.parse_args()
SIZE = args.size


######################################
# Generate parameters
######################################
PARAMETER_COLUMNS = ['P', 'beta', 'scale_beta', 't_inc', 'scale_ea', 't_i', 'delta', 'phi', 'scale_s']


def generate_multivariate_normal_parameters(dataframe):
    parameters_matrix = dataframe[PARAMETER_COLUMNS].values.T  # one row per parameter
    params_mean = parameters_matrix.mean(axis=1)
    params_cov = np.cov(parameters_matrix)
    return {"mean": params_mean, "cov": params_cov}


def generate_parameters_with_multivariate_normal(multivariate_parameters):
    return multivariate_normal.rvs(size=SIZE, **multivariate_parameters)


# Read the simulation output
parameters_path = "output/grouped_accepted_params_score.csv"
params_df = pd.read_csv(parameters_path)

# Generate samples with the multivariate normal
mvn = generate_multivariate_normal_parameters(params_df)
mvn_samples = generate_parameters_with_multivariate_normal(mvn)

# Reindex of the dataframe and store it in final parameters params.csv
output_path = 'final_parameters/data/params.csv'
df = pd.DataFrame(mvn_samples, columns=PARAMETER_COLUMNS)
df['id'] = range(1, SIZE + 1)
df = df[['id', *PARAMETER_COLUMNS]]
df.to_csv(output_path, index=False)
print(f'Parameters generated at: {output_path}. N: {SIZE}')
