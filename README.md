<!-- ABOUT THE PROJECT -->
## About The Project

This project collects a pipeline for using the Microscopic Markov Chain Approach from Alex Arenas for the Covid-19 spread disease. It includes the information for using this pipeline in a supercomputer as well as in your personal computer.


### Built With

The technologies involved in the pipeline are:
* [Python 3](https://www.python.org/download/releases/3.0/)
* [Julia Programming 1.3.1](https://julialang.org/)


<!-- Project structure -->
## Main pipeline

The usage of this package will be explained, as well as the distribution of the files and folders.

### Project structure

- **base/**: This is a folder that will be copied into a different instances in order to parallelize the computation
  * **data/**: Contain different values used in the model simulation
  * **run_parallel_simulation.jl**: Runs the Julia code for simulating the results for all the parameter set specified in `data` folder
  * **evaluate_score.py**: Compares the results from the simulation with the real date, and computes a score for each parameter set per CCAA
  * **run.sh**: Runs `run_parallel_simulation.jl` and executes `evaluate_score.py`
    

- **final_parameters/**: This folder contains the final epidemic values and results that will be used in further analysis
  

- **tools/**: Contains tools for analyzing the results of the model
  

- **prepare_instances.sh**: Create different instances from the `base` folder
  

- **generate_parameter_samples.py**: Create different sets of parameters for the epidemic model based on certain distributions
  

- **sbatch_all_instances.sh**: Executes with sbatch (use in a supercomputer) all the instances `run.sh` script. 
  

- **bash_all_instances.sh**: Executes with bash all the instances `run.sh` script
  

- **merge_instances_outputs.py**: Merge the output of each instance in a common folder in the root of the project.



### Simulation Pipeline

1. Clone the repo
   ```sh
   git clone https://github.com/your_username_/Project-Name.git
   ```
2. Install Python and Julia packages 
   ```sh
   # Purgue the MN4 and load correct versions for the languages
   module purge && module load gcc/5.4.0 mkl python/3.7.4 julia/1.3.1
   # Install the Python libraries you are missing
   pip3 install [pandas, ...]
   # Open Julia prompt
   julia
   # Open Pkg
   [
   # Create a project.toml
   activate .
   # Activates the environment
   instantiate
   # Install the packages there
   add DelimitedFiles
   add DataFrames
   add CSV
   add Printf
   add DataStructures
   add ProgressMeter
   add NPZ
   add MMCAcovid19
   add ArgParse
   # Precompile the libraries you have created
   precompile
   ```
3. Test your julia code is ready and operative:
    ```sh
   # The next code should execute the simulation for the params.csv
   # inside data folder in base folder. Once it's working, you are ready
   # for the next step
   
   cd base
   bash run.sh
   ```
4. Prepare instances for the simulation:
    ```sh
   # The next code should execute the simulation for the params.csv
   # inside data folder in base folder. Once it's working, you are ready
   # for the next step
   
   bash prepare_instances.sh -n N_INS -o INS_PATH -s SIZE -m mode
   
   N_INS: Number of instances to create (up to 48 on MN)
   INS_PATH: Folder to store instances ("instances/")
   SIZE: Number of simulations per instance (up to 300 000 on MN)
   MODE: "priors" or "posteriors". 
       "priors" will use the distributions on base/data/priors.json
       "posteriors" will use the previous simulation results
   
   ```

5. Run the instances in the supercomputer: 
    ```sh
   # Execute in the MN the instance run with sbatch_all_instances.sh script
   # You can use the bash_all_instances.sh in your computer for testing.
   
   sbatch sbatch_all_instances.sh
      
   ```
   
6. Merge instance outputs: 
    ```sh
   # Merge the outputs of all instances in a folder output in the root directory.
   # Also, filter the output score for a threshold. Exclude params with more errors.
   # than the threshold.
   
   python merge_instances_outputs.py
   
   # It creates a folder output with the following schema:
   - output/
      + accepted_prevalence.npy: 
          Prevalence of each CCAA during the time simulation per parameter set
      + accepted_params_score.csv:
          List of the parameter set with its score 
   ```

7. Accept the parameters:
    ```sh
   # Using different methods, accept the parameters based on the score for each CCAA.
   
   python accept_parameters.py  --mode=MODE --threshold=THRESHOLD
   
   MODE: "mmse" or "pareto". 
       "mmse" will use the error sum of all the CCAAs
       "pareto" will use the pareto front for filtering the parameters
   THRESHOLD: Depending on the method, this value exclude some parameters with more error
   N: Number of elements inside each group per Pareto method iteration.
   
   # It creates a folder output with the following schema:
   - output/ 
      + grouped_accepted_prevalence.npy
          Same than accetped_prevalence.npy but filtered by the threshold
      + grouped_accepted_params_score.csv
          Same than accepted_params_score.csv but filtered by the threshold
      + posteriors.csv
          Same than grouped_accepted_params_score.csv but preparing the format for the next simulation.
          This is the file that uses prepare_instances on mode "posteriors".
   ```

8. Calculate final distribution for analysis:
    ```sh
   # Generate a final set of parameters with a multivariate normal distribution
   # on final_parameters/data folder
   
   python calculate_final_parameters.py --size=SIZE
   
   SIZE: Number of parameters to analyze
   
   ```

9. Run the parameters in final_parameters with run.sh script.
    ```sh
   # It creates a folder output in the final_parameters folder. This files
   # can be analyzed with tools' scripts.
   bash run.sh
   ```

### Analysis Pipeline

There are three Python notebooks in order to analyze the simulation results in the `tools` folder:

1. **analyze_output_parameters.ipynb**

    Gets the parameter score of all the simulations and build the distribution of them, the correlation and more.


2. **analyze_parameters_on_ccaa.ipynb**
   
    Analyzes the parameters with the prediction curves for the CCAAs.


3. **analyze_prediction.ipynb**
 
    Uses the best parameters in the `final_parameters` folder for forecasting the prevalence curve.


4. **analyze_pareto_method.ipynb**

    Shows the usability of the Pareto method package in an academic example