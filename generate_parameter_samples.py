import os
import glob
import argparse
import json
import pandas as pd
import numpy as np

from scipy.stats import uniform, multivariate_normal


######################################
# Parser options
######################################
ap = argparse.ArgumentParser(description="")

ap.add_argument('--path', dest="path", required=True, help='Path to the new instance')
ap.add_argument('--size', dest="size", required=True, type=int, help='Number of samples to generate')
ap.add_argument('--mode', dest="mode", required=True, type=str, help='Mode of generation (by priors or posteriors)')

args = ap.parse_args()

instances_path = args.path
SIZE = args.size
mode = args.mode


######################################
# MODE 1: With PRIORS
######################################
def sample_uniform(loc, scale):
	return uniform.rvs(loc=loc, scale=scale, size=SIZE)


def generate_parameters_with_priors(config):
	parameters = {
		'id': list(range(1, SIZE+1)),
	}
	columns = ['id']

	for param_def in config['parameters']:

		if param_def['type'] == 'constant':
			parameters[param_def['name']] = [param_def['args']['value']] * SIZE
			columns.append(param_def['name'])

		elif param_def['type'] == 'uniform':
			parameters[param_def['name']] = sample_uniform(**param_def['args'])
			columns.append(param_def['name'])

		else:
			print('WARN: unrecognized parameter type, ', param_def['type'])

	return pd.DataFrame(parameters, columns=columns)


def populate_instances_with_parameters_from_priors(instances):
	priors_path = "base/data/priors.json"
	with open(priors_path) as f:
		config = json.load(f)

	for n, instance in enumerate(instances):
		output_path = os.path.join(instance, 'data', 'params.csv')
		df = generate_parameters_with_priors(config)
		df.to_csv(output_path, index=False)
		print('PRIOR generator => Writing output to: ', output_path)

	exit()


###################################################
# MODE 2: With POSTERIORS
###################################################
def generate_multivariate_normal_parameters(dataframe):
	parameters_columns = [p for p in dataframe.columns if p not in ['id', 'score']]
	parameters_matrix = dataframe[parameters_columns].values.T  # one row per parameter
	params_mean = parameters_matrix.mean(axis=1)
	params_cov = np.cov(parameters_matrix)
	return {"mean": params_mean, "cov": params_cov}


def generate_parameters_with_multivariate_normal(multivariate_parameters):
	return multivariate_normal.rvs(size=SIZE, **multivariate_parameters)


def populate_instances_with_parameters_from_posteriors(instances):
	posteriors_path = "output/posteriors.csv"
	if not os.path.isfile(posteriors_path):
		print('Posterior file missing on: ', posteriors_path)
		exit()

	posterior_df = pd.read_csv(posteriors_path)
	parameter_columns = [p for p in posterior_df.columns if p not in ['id', 'score']]
	mvn = generate_multivariate_normal_parameters(posterior_df)
	for n, instance in enumerate(instances):
		output_path = os.path.join(instance, 'data', 'params.csv')
		mvn_samples = generate_parameters_with_multivariate_normal(mvn)
		df = pd.DataFrame(mvn_samples, columns=parameter_columns)
		df['id'] = range(1, SIZE + 1)
		df = df[['id', *parameter_columns]]
		df.to_csv(output_path, index=False)
		print('PRIOR generator => Writing output to: ', output_path)

	exit()


################################################
# Iter the instances and populate the parameters
################################################
instances_path_list = glob.glob(instances_path)
if mode == 'priors':
	population_method = populate_instances_with_parameters_from_priors
else:
	population_method = populate_instances_with_parameters_from_posteriors

population_method(instances_path_list)
