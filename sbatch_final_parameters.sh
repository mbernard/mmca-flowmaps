#!/bin/bash
#SBATCH --job-name=EMMCAACCEPTPARAMETERS
#SBATCH --workdir=.
#SBATCH --output=output_%j.out
#SBATCH --error=output_%j.err
#SBATCH --ntasks=1
#SBATCH --qos=debug
#SBATCH --cpus-per-task=48
#SBATCH --time=01:59:00

export JULIA_NUM_THREADS=48
module load julia/1.3.1

# Run Julia code inside final_parameters
srun -n1 -N1 --exclusive bash final_parameters/run.sh &
wait
