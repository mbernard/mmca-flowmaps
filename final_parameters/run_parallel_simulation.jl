using DelimitedFiles
# using Statistics
using DataFrames
using CSV
using Printf
using DataStructures
using Base.Threads
using ProgressMeter
using NPZ
using MMCAcovid19
using ArgParse

# println(pathof(MMCAcovid19))

slurm_job_id = get(ENV, "SLURM_JOB_ID", "None")
println("slurm_job_id:", slurm_job_id)


## -----------------------------------------------------------------------------
## LOADING DATA
## -----------------------------------------------------------------------------

# Path data
data_path = "data/"
output_path = "output/"

# Loading mobility network
network = CSV.read(@sprintf("%s/R_mobility_matrix.csv", data_path), DataFrame)

edgelist = convert(Array{Int64, 2}, network[:, 1:2]) .+ 1
Rᵢⱼ = copy(network[:, 3])

# Patch surface
sᵢ = CSV.read(@sprintf("%s/S_patch_surface_area.csv", data_path), DataFrame)[:,"area"]

# Population info
nᵢ_ages = CSV.read(@sprintf("%s/n_population_patch_age.csv", data_path), DataFrame);
conditions₀ = CSV.read(@sprintf("%s/A0_initial_conditions_tunned.csv", data_path), DataFrame)

C = readdlm(@sprintf("%s/C_age_contact_matrix.csv", data_path),
            ',', Float64)

# ABC Parameters
# paramsDF = CSV.read(@sprintf("%s/params_best_fit_sampled.csv", data_path), DataFrame)
paramsDF = CSV.read(@sprintf("%s/params.csv", data_path), DataFrame)

# Containement measures
κ₀_df = CSV.read(@sprintf("%s/kappa0_by_time.csv", data_path), DataFrame)

# Patch to CCAA mapping matrix
PatchToCCAA = npzread(@sprintf("%s/patch_to_ccaa.npy", data_path))
n_ccaa = size(PatchToCCAA)[1]


## -----------------------------------------------------------------------------
## SETTING PARAMETERS (some of them are just placeholders for the values on DF)
## -----------------------------------------------------------------------------


## POPULATION

# Patch population for each strata
nᵢᵍ = copy(transpose(convert(Array{Float64,2}, nᵢ_ages[:,["Y", "M", "O"]])))

# Total patch population
nᵢ = convert(Array{Float64,1}, nᵢ_ages[:,"Total"])

# Num. of patches
M = length(nᵢ)

# Num of stratas
G = size(C)[1]

# Average number of contacts per strata
kᵍ = [11.8, 13.3, 6.76]

# Average number of contacts at home per strata
kᵍ_h = [3.15, 3.17, 3.28]

# Average number of contacts at work per strata
kᵍ_w = [1.72, 5.18, 0.0]

# Degree of mobility per strata
pᵍ = [0.0, 1.0, 0.00]

# Density factor
ξ = 0.01

# Average household size
σ = 2.5

# Check network structure and self-loop correction
edgelist, Rᵢⱼ = MMCAcovid19.correct_self_loops(edgelist, Rᵢⱼ, M)


# population_params = Population_Params(G, M, nᵢᵍ, kᵍ, kᵍ_h, kᵍ_w, C, pᵍ, edgelist, Rᵢⱼ, sᵢ, ξ, σ)


## EPIDEMIC PARAMETERS

# Scaling of the asymptomatic infectivity
scale_β = 0.51

# Infectivity of Symptomatic
βᴵ = 0.0903

# Infectivity of Asymptomatic
βᴬ = scale_β * βᴵ

# Exposed rate
ηᵍ = [1/3.64, 1/3.64, 1/3.64]

# Asymptomatic rate
αᵍ = [1/3.76, 1/1.56, 1/1.56]

# Infectious rate
μᵍ = [1/1.0, 1/3.2, 1/3.2]

# Direct death probability
θᵍ = [0.00, 0.008, 0.047]

# Direct death probability
γᵍ = [0.0003, 0.003, 0.026]

# Pre-deceased rate
ζᵍ = [1/7.8, 1/7.8, 1/7.8]

# Pre-hospitalized in ICU rate
λᵍ = ones(Float64, 3)

# Fatality probability in ICU
ωᵍ = [0.30, 0.30, 0.30]

# Death rate in ICU
ψᵍ = [1/7.0, 1/7.0, 1/7.0]

# ICU discharge rate
χᵍ = [1/21.0, 1/21.0, 1/21.0]

# Number of timesteps
# dia inicial: 9 Feb
# dia final: 14 Abril (66 dias)
# T = 123
T = 66

# Epidemic parameters
# epi_params = Epidemic_Params(βᴵ, βᴬ, ηᵍ, αᵍ, μᵍ, θᵍ, γᵍ, ζᵍ, λᵍ, ωᵍ, ψᵍ, χᵍ, G, M, T)

## CONFINEMENT MEASURES

# Timesteps when the containment measures will be applied
tᶜs = κ₀_df.time[:]

# Array of level of confinement
κ₀s = κ₀_df.reduction[:]

# Array of premeabilities of confined households
ϕs = ones(Float64, length(tᶜs))

# Array of social distancing measures
δs = ones(Float64, length(tᶜs))


## INITIALIZATION OF THE EPIDEMICS

# Initial number of exposed individuals
E₀ = zeros(G, M)

# Initial number of infectious asymptomatic individuals
A₀ = zeros(Float64, G, M)

# Distribution of the intial infected individuals per strata
A₀[1, Int.(conditions₀[:,"idx"]) .+ 1] .= 0.12 .* conditions₀[:,"seed"]
A₀[2, Int.(conditions₀[:,"idx"]) .+ 1] .= 0.16 .* conditions₀[:,"seed"]
A₀[3, Int.(conditions₀[:,"idx"]) .+ 1] .= 0.72 .* conditions₀[:,"seed"]

# Initial number of infectious symptomatic individuals
I₀ = zeros(Float64, G, M)



## -----------------------------------------------------------------------------
## SETTING UP SIMULATION VARIABLES
## -----------------------------------------------------------------------------

# Num. of set of epidemic parameters
num_params = length(paramsDF.id)
# num_params = 3

println("Allocating output matrices (size = n_ccaa x T x num_params):")
println("num_params: ", num_params)
println("n_ccaa: ", n_ccaa)
println("T: ", T)

# Arrays where the output variables will be stored
incidence  = zeros(Float64, n_ccaa, T - 1, num_params)
prevalence = zeros(Float64, n_ccaa, T, num_params)
deaths = zeros(Float64, n_ccaa, T, num_params)
deaths_new = zeros(Float64, n_ccaa, T - 1, num_params)

## SETTING UP THE THREADING VARIABLES

# Number of threads used to parallelize the execution
# nThreads = 48;
nThreads = nthreads();
# nThreads = 1;
println("nThreads: ", nThreads)
flush(stdout)

# Circular queues to optimize and reuse the population and epidemic structures
populations = CircularDeque{Population_Params}(nThreads)
epi_params = CircularDeque{Epidemic_Params}(nThreads)

# Populate the circular deque
for t in 1:nThreads
    push!(populations, Population_Params(G, M, nᵢᵍ, kᵍ, kᵍ_h, kᵍ_w, C, pᵍ, edgelist, Rᵢⱼ, sᵢ, ξ, σ))
    push!(epi_params, Epidemic_Params(βᴵ, βᴬ, ηᵍ, αᵍ, μᵍ, θᵍ, γᵍ, ζᵍ, λᵍ, ωᵍ, ψᵍ, χᵍ, G, M, T))
end


## -----------------------------------------------------------------------------
## FUNCTIONS
## -----------------------------------------------------------------------------

"""
    run_simu_params!(epi_params::Epidemic_Params,
                     population::Population_Params,
                     paramsdf::dataframe,
                     indx_id::int64,
                     a₀::array{float64, 2},
                     i₀::array{float64, 2},
                     incidence::array{float64, 2},
                     prevalence::array{float64, 2},
                     deaths_new::array{float64, 2},
                     deaths::array{float64, 2})

Runs a SEAIRHD simulation on a specific set of parameters stored in the DF
updateing the tables of prevalence, incidence, num. of deaths and daily deaths.
"""
function run_simu_params!(epi_params::Epidemic_Params,
                          population::Population_Params,
                          paramsDF::DataFrame,
                          indx_id::Int64,
                          A₀::Array{Float64, 2},
                          I₀::Array{Float64, 2},
                          incidence::Array{Float64, 3},
                          prevalence::Array{Float64, 3},
                          deaths_new::Array{Float64, 3},
                          deaths::Array{Float64, 3})

    # Loading parameters from DF
    id, P, β, scale_β, τ_inc, scale_ea, τᵢ, δ, ϕ, scale₀ = paramsDF[indx_id, :]

    # Set epidemic params to the ones speficied on the DF
    epi_params.βᴬ .= scale_β * β
    epi_params.βᴵ .= β
    epi_params.ηᵍ .= 1.0/(τ_inc * (1.0 - scale_ea))
    epi_params.αᵍ .= [1.0/(τᵢ - 1 + τ_inc * scale_ea),
                      1.0/(τ_inc * scale_ea),
                      1.0/(τ_inc * scale_ea)]
    epi_params.μᵍ .= [1.0, 1.0/τᵢ, 1.0/τᵢ]

    # Reset compartments
    reset_params!(epi_params, population)
    set_initial_infected!(epi_params, population, E₀, scale₀ .* A₀, I₀)

    # Set containment parameters
    ϕs .= ϕ
    δs .= δ

    ## RUN EPIDEMIC SPREADING
    run_epidemic_spreading_mmca!(epi_params, population, tᶜs, κ₀s, ϕs, δs, verbose=false)


    # Compute the prevalence
    prevalence[:, :, indx_id] = PatchToCCAA * sum((epi_params.ρᴵᵍ[:, :, 1:epi_params.T] .+
                                   epi_params.ρᴴᴰᵍ[:, :, 1:epi_params.T] .+
                                   epi_params.ρᴴᴿᵍ[:, :, 1:epi_params.T] .+
                                   epi_params.ρᴾᴴᵍ[:, :, 1:epi_params.T] .+
                                   epi_params.ρᴾᴰᵍ[:, :, 1:epi_params.T] .+
                                   epi_params.ρᴰᵍ[:, :, 1:epi_params.T] .+
                                   epi_params.ρᴿᵍ[:, :, 1:epi_params.T] .+
                                   epi_params.ρᴬᵍ[:, :, 1:epi_params.T] .+
                                   epi_params.ρᴱᵍ[:, :, 1:epi_params.T]) .*
                                  population.nᵢᵍ, dims = (1))[1,:,:]  # sum by CCAA

    # # Compute the prevalence
    # prevalence[:, indx_id] .= sum((epi_params.ρᴵᵍ[:, :, 1:epi_params.T] .+
    #                                epi_params.ρᴴᴰᵍ[:, :, 1:epi_params.T] .+
    #                                epi_params.ρᴴᴿᵍ[:, :, 1:epi_params.T] .+
    #                                epi_params.ρᴾᴴᵍ[:, :, 1:epi_params.T] .+
    #                                epi_params.ρᴾᴰᵍ[:, :, 1:epi_params.T] .+
    #                                # epi_params.ρᴿᵍ[:, :, 1:epi_params.T] .+
    #                                # epi_params.ρᴰᵍ[:, :, 1:epi_params.T] .+
    #                                epi_params.ρᴬᵍ[:, :, 1:epi_params.T] .+
    #                                epi_params.ρᴱᵍ[:, :, 1:epi_params.T]) .*
    #                               population.nᵢᵍ, dims = (1, 2))[1, 1, :]

    # Compute the incidence
    incidence[:, :, indx_id]  = diff(prevalence[:, :, indx_id], dims=2)

    # Compute total number of deaths
    deaths[:, :, indx_id] = PatchToCCAA * sum(epi_params.ρᴰᵍ[:, :, 1:epi_params.T] .*
                                          population.nᵢᵍ, dims = (1))[1,:,:]  # sum by CCAA

    # Compute daily new deaths
    deaths_new[:, :, indx_id] = diff(deaths[:, :, indx_id], dims=2)

end


## -----------------------------------------------------------------------------
## RUN THE SIMULATION
## -----------------------------------------------------------------------------

# Setup progress bar
p = Progress(num_params)

# Circular Deque Lock
lockS = SpinLock()

# Run the simulation for all the parameters
@threads for indx_id in 1:num_params
    
    # println("Thread: ", threadid(), " row: ", indx_id)

    # Lock the data structure
    lock(lockS)

    # Recover population and epidemic parameters
    population = pop!(populations)
    epi_param = pop!(epi_params)

    # Unlock data structure
    unlock(lockS)

    # Run simu
    run_simu_params!(epi_param,
                     population,
                     paramsDF,
                     indx_id,
                     A₀,
                     I₀,
                     incidence,
                     prevalence,
                     deaths_new,
                     deaths)

    # Lock the data structure
    lock(lockS)

    # Free population and epidemic parameters
    push!(populations, population)
    push!(epi_params, epi_param)

    # Unlock data structure
    unlock(lockS)

    # Update progress bar
    next!(p)
    
end


## -----------------------------------------------------------------------------
## STORING THE RESULTS
## -----------------------------------------------------------------------------

mkpath(output_path)

npzwrite(@sprintf("%s/incidence.npy", output_path), incidence)
npzwrite(@sprintf("%s/prevalence.npy", output_path), prevalence)
npzwrite(@sprintf("%s/deaths.npy", output_path), deaths)
npzwrite(@sprintf("%s/deaths_new.npy", output_path), deaths_new)
