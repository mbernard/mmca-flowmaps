#!/bin/bash

cd "$(dirname "$0")"

julia --project=. run_parallel_simulation.jl
