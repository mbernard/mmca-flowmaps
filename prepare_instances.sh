#!/bin/bash
set -e

usage() {
  echo "Usage: $0 [-n <int> number of instances]
                  [-o <string> output folder]
                  [-s <int> number of simulations per task]
                  [-m <string> mode of creation (priors or posteriors)]"
                1>&2; exit 1;
}

while getopts ":n:o:s:m:" option; do
    case "${option}" in
        n)
            n=${OPTARG}
            ;;
        o)
            o=${OPTARG}
            ;;
        s)
            s=${OPTARG}
            ;;
        m)
            m=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${n}" ] || [ -z "${o}" ] || [ -z "${s}" ] || [ -z "${m}" ]; then
    usage
fi


NUM_INSTANCES=$n
OUTPUT_PATH=$o
SIMULATIONS_PER_TASK=$s
MODE_OF_GENERATION=$m
INSTANCE_PREFIX="instance"

echo "NUM_INSTANCES = ${NUM_INSTANCES}"
echo "OUTPUT_PATH = ${OUTPUT_PATH}"
echo "SIMULATIONS_PER_TASK = ${SIMULATIONS_PER_TASK}"
echo "MODE_OF_GENERATION = ${MODE_OF_GENERATION}"

for i in $(seq 1 $NUM_INSTANCES); do
	instance_path=${OUTPUT_PATH}${INSTANCE_PREFIX}$i
	echo $instance_path

	mkdir -p $instance_path

	cp -r base/* $instance_path

	echo "Generating parameter sample..."
	python3 generate_parameter_samples.py --path $instance_path --size $SIMULATIONS_PER_TASK --mode=$MODE_OF_GENERATION

	echo -e "Done\n"
done
