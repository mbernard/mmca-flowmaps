import pandas as pd
import numpy as np
import argparse
import time
import threading
from paretoset import paretoset


######################################
# Parser options
######################################
ap = argparse.ArgumentParser(description="")
ap.add_argument('--mode', dest="mode", required=True, type=str, help='Mode of calculation the accepted parameters')
ap.add_argument('--threshold', dest="threshold", required=False, type=int, help='Threshold for filtering the param score')
ap.add_argument('--N', dest="N", required=False, type=int, help='Number of elements inside each group per Pareto method iteration')
args = ap.parse_args()


######################################
# Accept/reject simulations
######################################
filename = 'output/accepted_prevalence.npy'
S = np.load(filename)

filename = 'output/accepted_params_score.csv'
params_df = pd.read_csv(filename)

if args.mode == 'mmse':
    """
    MMSE METHOD:
        Computes the sum for every CCAA and asign this score to the parameter set.
    """
    print("Accepting/rejecting simulations by MMSE method")

    params_df['score'] = params_df[['score_ccaa_' + str(i) for i in range(1, 20)]].sum(axis=1)
    under_threshold = params_df['score'] <= args.threshold
    accepted_params_score = params_df[under_threshold]
    accepted_S = S[:, :, under_threshold]

    print(f"ACCEPTED SIMULATIONS: {accepted_params_score.shape[0]} of {params_df.shape[0]}")

    if accepted_params_score.empty:
        print(f'All parameters rejected for the chosen threshold={args.threshold}')
        exit()

else:
    """
    PARETO FRONT METHOD:
        In this case, we will use the Pareto front method for accepting parameters. The idea is that we select
        those parameters that have a better score in each direction (CCAA score). So the method tries to find
        an equilibrium between all the CCAAs.
    """
    print("Accepting/rejecting simulations by Pareto front method")

    def filter_set_by_pareto(params_for_pareto, prevalence_for_pareto, params, prevalence, i, dimension=19):
        params_without_columns = params_for_pareto[['score_ccaa_' + str(column + 1) for column in range(dimension)]]
        mask = paretoset(params_without_columns, sense=["min"] * dimension)
        params[i] = params_for_pareto[mask]
        prevalence[i] = prevalence_for_pareto[:, :, mask]

    d = 19  # Number of autonomous communities in csv file
    N = args.N  # Group for pareto method

    # We will make a while loop allowing the Pareto working with
    # subsets of the parameters. After that, group till the accuracy
    # is reached or maximum iterations are reached.
    iterations = 0
    max_iterations = 20
    max_minutes = 60
    has_to_finish = False
    accepted_params_score = params_df
    accepted_S = S
    absolute_ti = time.time()
    while iterations < max_iterations and not has_to_finish and (time.time() - absolute_ti)/60 < max_minutes:
        ti = time.time()
        print(f"\nIteration {iterations + 1}. Total records to proccess {accepted_params_score.shape[0]}", flush=True)
        if accepted_params_score.shape[0] < N:
            print("Converged. Going for the last iteration", flush=True)
            has_to_finish = True

        # Shuffle the records
        idx = np.random.permutation(accepted_params_score.index)
        accepted_params_score = accepted_params_score.reindex(idx)
        accepted_S = accepted_S[:, :, idx]

        # We create a list of indexes where the pareto method will
        # separate in threads the whole parameter set
        pair_groups = [(N * value, N * (value + 1)) for value in range(int(accepted_params_score.shape[0]/N) + 1)]
        threads = [None] * len(pair_groups)
        params_result = [None] * len(pair_groups)
        prevalence_result = [None] * len(pair_groups)
        for i, (min_index, max_index) in enumerate(pair_groups):
            thread = threading.Thread(
                target=filter_set_by_pareto,
                args=(
                    accepted_params_score[min_index:max_index],
                    accepted_S[:, :, min_index:max_index],
                    params_result,
                    prevalence_result,
                    i
                )
            )
            threads[i] = thread
            threads[i].start()

        # Wait all threads to finish
        for i in range(len(threads)):
            min_index, max_index = pair_groups[i]
            threads[i].join()
            print(f"Thread {i + 1} of {len(threads)} done. "
                  f"Records accepted: {params_result[i].shape[0]} of {accepted_params_score[min_index:max_index].shape[0]}. "
                  f"Time elapsed: {(time.time() - ti)/60} minutes.",
                  flush=True)

        # Concat the results
        accepted_params_score = []
        accepted_S = []
        for params, prev in zip(params_result, prevalence_result):
            accepted_params_score.append(params)
            accepted_S.append(prev)
        accepted_params_score = pd.concat(accepted_params_score, axis=0, ignore_index=True)
        accepted_S = np.concatenate(accepted_S, axis=2)

        iterations += 1

    print(2*"\n", "Elapsed time for Pareto method: ", (time.time() - ti)/60, "minutes")


# Write the accepted parameters in a csv
filename = 'output/grouped_accepted_params_score.csv'
print(f'Saving file: {filename}')
accepted_params_score.loc[:, 'id'] = range(1, accepted_params_score.shape[0] + 1)
accepted_params_score.set_index('id', inplace=True)
accepted_params_score.to_csv(filename)


# Write the accepted prevalence in a npy file as well
filename = 'output/grouped_accepted_prevalence.npy'
print(f'Saving file: {filename}')
with open(filename, 'wb') as f:
    np.save(f, accepted_S)


##################################################
# Prepare the posterior for the next ABC iteration
##################################################
filename = 'output/posteriors.csv'
print(f'Saving file: {filename}')
posteriors = accepted_params_score.loc[:, ('full_idx', 'P', 'beta', 'scale_beta', 't_inc', 'scale_ea', 't_i', 'delta', 'phi', 'scale_s')]
posteriors.rename(columns={'full_idx': 'id'}, inplace=True)
posteriors['id'] = range(1, posteriors.shape[0] + 1)
posteriors.set_index('id', inplace=True)
posteriors.to_csv(filename)
