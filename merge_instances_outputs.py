import glob
import pandas as pd
import numpy as np
import os
import argparse


######################################
# Get the data from instances folders
######################################
instances = glob.glob('instances/*')

# We store the prevalence and the param score for each parameter
parameters = []
matrices = []
for n, instance in enumerate(instances):
    print('- adding instance: ', instance)

    matrix_path = f"{instance}/output/accepted_prevalence.npy"
    if not os.path.isfile(matrix_path):
        print('- missing prevalence.npy matrix for instance: ', instance)
        continue
    mat = np.load(matrix_path)

    params_path = f"{instance}/output/accepted_params_score.csv"
    if not os.path.isfile(params_path):
        print('- missing params file for instance: ', instance)
        continue
    par = pd.read_csv(params_path)
    par['instance'] = instance
    par['instance_idx'] = range(par.shape[0])
    par['instance_n'] = n

    assert par.shape[0] == mat.shape[2]

    matrices.append(mat)
    parameters.append(par)

params_df = pd.concat(parameters, axis=0, ignore_index=True)
index = params_df.index
index.name = "full_idx"
S = np.concatenate(matrices, axis=2)


######################################
# write output
######################################
filename = 'output/accepted_prevalence.npy'
with open(filename, 'wb') as f:
    np.save(f, S)

filename = 'output/accepted_params_score.csv'
print(f'Saving file: {filename}')
params_df.to_csv(filename)

exit()
