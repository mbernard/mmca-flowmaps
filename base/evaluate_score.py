import argparse
import numpy as np
import pandas as pd
import time

from datetime import datetime
from paretoset import paretoset


######################################
# Parser options
######################################
ap = argparse.ArgumentParser(description="")

ap.add_argument('--first-date', dest="first_date", required=True, type=str, help='First date of simulation')
ap.add_argument('--last-date', dest="last_date", required=True, type=str, help='Last date of simulation')
ap.add_argument('--mode', dest="mode", required=True, type=str, help='Mode for filtering the CCAA score from the set')
ap.add_argument('--threshold', dest="threshold", required=False, type=int, help='Threshold for filtering the param score')

args = ap.parse_args()


######################################
# Load real data
######################################
N_CCAA = 19
COLUMN_FOR_FITTING_REAL_DATA = 'total_cases'
REAL_DATA_PATH = 'data/real_covid_data.csv'
start_date = datetime.strptime(args.first_date, "%Y-%m-%d")
end_date = datetime.strptime(args.last_date, "%Y-%m-%d")
num_days = (end_date-start_date).days + 1

# Filter for simulation period
real_df = pd.read_csv(REAL_DATA_PATH)
real_df = real_df[real_df['date'] >= args.first_date]
real_df = real_df[real_df['date'] <= args.last_date]

# Convert to matrix (ncca x num_days; value=total_cases)
real_matrix = real_df[['id', 'date', COLUMN_FOR_FITTING_REAL_DATA]].sort_values(['id', 'date']).set_index(['id', 'date']).unstack().values

assert real_matrix.shape == (N_CCAA, num_days), \
    "Number of dates in real data doesn't match with number of dates of the simulation" \
    " or number of CCAA doesn't match with theoretical number of CCAA"


######################################
# Load simulation results
######################################
COLUMN_FOR_FITTING_SIMULATION = 'prevalence'
PARAMS_PATH = "data/params.csv"
params_df = pd.read_csv(PARAMS_PATH)

# Shape of simulation matrix => (#CCAA_ids x #dates x #different_parameters)
simulation_matrix = np.load(f"output/{COLUMN_FOR_FITTING_SIMULATION}.npy")
simulation_matrix = simulation_matrix[:, :num_days, :]

assert real_matrix.shape == simulation_matrix.shape[:2], \
    f"ERROR: cannot compare simulation results with real data, dimensions do not match " \
    f"(ids, dates): real_matrix: {real_matrix.shape}, simulation_matrix: {simulation_matrix.shape[:2]}"


######################################
# Compute score
######################################
print('Computing MSE')

M = simulation_matrix
R = real_matrix[..., np.newaxis]
assert len(M.shape) == len(R.shape), "M and R has not the same shape length"

# Rescale difference dividing by max/mean value in the real time serie
# [Diferencia porcentual respecto al valor medio/mayor de la serie]
R_max = np.max(R, axis=1, keepdims=True)
D = (M-R)/R_max
SE = np.square(D)

# MSE[:, 0]  # MSE for all CCAA, for parameters 0
# MSE[:, MMSE.argmax()]   # MSE for all CCAA, for worst parameters
# MSE[:, MMSE.argmin()]   # MSE for all CCAA, for best parameters
MSE = SE.sum(axis=1) / SE.shape[1]

"""
We are trying to evaluate the score using the score for each CCAA
MMSE = MSE.sum(axis=0) / MSE.shape[0]

print("- MMSE min: ", MMSE.min())
print("- MMSE max: ", MMSE.max())
"""

######################################
# Assign score to each parameter set
######################################
"""
params_df['score'] = MMSE
"""

params_df = pd.concat([
    params_df,
    pd.DataFrame(MSE.transpose(),
                 index=params_df.index,
                 columns=['score_ccaa_' + str(i) for i in range(1, 20)])
], axis=1)

######################################
# write output
######################################
SCORE_PATH = 'output/params_score.csv'

print(f'Writing parameters with score to {SCORE_PATH}. Shape: {params_df.shape}')
params_df.to_csv(SCORE_PATH, index=False)


######################################
# Filter the params
######################################
filename = 'output/prevalence.npy'
S = np.load(filename)

filename = 'output/params_score.csv'
params_df = pd.read_csv(filename)


if args.mode == 'mmse':
    """
    MMSE METHOD:
        Computes the sum for every CCAA and asign this score to the parameter set.
    """
    print("Accepting/rejecting simulations by MMSE method")

    params_df['score'] = params_df[['score_ccaa_' + str(i) for i in range(1, 20)]].sum(axis=1)
    under_threshold = params_df['score'] <= args.threshold
    accepted_params_score = params_df[under_threshold]
    accepted_S = S[:, :, under_threshold]

    print(f"ACCEPTED SIMULATIONS: {accepted_params_score.shape[0]} of {params_df.shape[0]}")

    if accepted_params_score.empty:
        print(f'All parameters rejected for the chosen threshold={args.threshold}')
        exit()

else:
    """
    PARETO FRONT METHOD:
        In this case, we will use the Pareto front method for accepting parameters. The idea is that we select
        those parameters that have a better score in each direction (CCAA score). So the method tries to find
        an equilibrium between all the CCAAs.
    """
    print("Accepting/rejecting simulations by Pareto front method")

    ti = time.time()
    d = 19  # Number of autonomous communities in csv file
    params_for_pareto = params_df[['score_ccaa_' + str(column + 1) for column in range(d)]]
    mask = paretoset(params_for_pareto, sense=["min"]*d)
    accepted_params_score = params_df[mask]
    accepted_S = S[:, :, mask]
    print("Elapsed time for Pareto method: ", time.time() - ti)


# Write the accepted parameters in a csv
filename = 'output/accepted_params_score.csv'
print(f'Saving file: {filename}')
accepted_params_score.loc[:, 'id'] = range(1, accepted_params_score.shape[0] + 1)
accepted_params_score.set_index('id', inplace=True)
accepted_params_score.to_csv(filename)


# Write the accepted prevalence in a npy file as well
filename = 'output/accepted_prevalence.npy'
print(f'Saving file: {filename}')
with open(filename, 'wb') as f:
    np.save(f, accepted_S)
