#!/bin/bash

cd "$(dirname "$0")"

julia --project=. run_parallel_simulation.jl

python3 evaluate_score.py --first-date="2020-02-09" --last-date="2020-04-14" --mode="pareto"
